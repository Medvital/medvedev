package Homework;

import javax.swing.*;



public class Recurs {
    public static int pow(int a, int b) {
        a = 2;
        b = Integer.valueOf(JOptionPane.showInputDialog("Введите степень n:"));
        for(; b > 0; b--){
            System.out.println(Math.pow(a, b));
        }
        return 1;
    }
    public static void main(String[] args){
        /*Вывод последовательности от 2^n до 1 через
        через рекурсию с n, указанным пользователем */
        System.out.println(pow(2, 0));
    }
}

/*
                public static int pow(int x, int y) {
// Простой пример рекурсии.
class Factorial {
// это рекурсивный метод
int fact(int n) {
int result;
if(n==l) return 1;
result = fact(n-l) * n;
return result;
class Recursion {
public static void main(String args [ ]) {
Factorial f = new Factorial ();
System.out.println("Факториал 3 равен " + f.fact(3));
System.out.println("Факториал 4 равен " + f.fact(4));
System.out.println("Факториал 5 равен ".+ f.fact(5));
}
}
 */