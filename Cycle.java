package Homework;

        import static java.lang.Math.pow;
        import javax.swing.*;

public class Cycle {
    public static void main(String[] args){
        /*Вывод последовательности от 2^n до 1 через
        через  цикл с n, указанным пользователем */
        int a, b;
        a = 2;
        b = Integer.valueOf(JOptionPane.showInputDialog("Введите степень n:"));
        for(;b > 0; b--){
            System.out.println(Math.pow(a, b));
        }
        System.out.println(1);
    }
}
